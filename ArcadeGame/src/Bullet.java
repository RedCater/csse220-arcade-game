import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.io.File;

public class Bullet extends Entity{
	int posX;
	int posY;
	int velocityX; //does an egg need velocity?
	int velocityY; //or can it just get its position set by the buzzard carrying it;
	File texture;
	boolean hasCollided = false;
	public Bullet(int startX, int startY) {
		posX = startX;
		posY = startY;
		
	}
	
	@Override
	protected void update() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected boolean shouldDestroy() {
		
		return hasCollided;
	}

	@Override
	public Rectangle getHitbox() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColliableType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void doCollision(Rectangle hitBox, String collidableType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void drawOn(Graphics2D g) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
