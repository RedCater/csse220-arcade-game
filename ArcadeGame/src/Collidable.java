import java.awt.Rectangle;

public interface Collidable {
	public Rectangle getHitbox();
	public String getColliableType(); // probably not a string
	public void doCollision(Rectangle hitBox, String collidableType);

}
