import java.awt.Graphics2D;

public interface Drawable {
	
	public void drawOn(Graphics2D g);

}
